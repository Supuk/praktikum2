$(document).ready(function () {
    //KEY : VALUE
    var language = {
        'eng': {
            'prijava': "Sign in",
            'zapomni': "Remember Me",
            'novracun': "Don't have an account yet? Sign up!",
            'geslo': "Password:",
            'email': "Enter Email Address:",
            'sledeni': "Tracked",
            'sledilci': "Followers",
            'zgodovina': "My history",
            'odjava': "Sign out",
            'registrirajse': "Sign up",
            'registrirajracun': "Register Account",
            'obstaja': "Already have an account? Login!",

        },
        'svn': {
            'prijava': "Prijavi se",
            'zapomni': "Zapomni si me",
            'novracun': "Še nimaš računa? Registriraj se!",
            'geslo': "Vnesite geslo:",
            'email': "Vnesite svojo E-pošto:",
            'sledeni': "Sledeni",
            'sledilci': "Sledilci",
            'zgodovina': "Moja zgodovina",
            'odjava': "Odjavi se",
            'registrirajse': "Registriraj se",
            'registrirajracun': "Registriraj se",
            'obstaja': "Že imate račun? Prijavite se!",


        }
    };
    $(function () {
        $('.translate').click(function () {
            var lang = $(this).attr('id');
            console.log(lang);
            setCookie('lang', lang, 30);
            //document.cookie = lang;
            $('.lang').each(function (index, item) {
                $(this).text(language[lang][$(this).attr('key')]);
            });

        })
        lang = getCookie('lang');
        $('.lang').each(function (index, item) {
            $(this).text(language[lang][$(this).attr('key')]);
        });
    });

});
