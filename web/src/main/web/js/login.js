function login() {

    const email = document.getElementById("email_field");
    const password = document.getElementById("password_field");
    // window.alert(email + password);

    const eemail = email.value;
    const pass = password.value;
    const auth = firebase.auth();

    const promise = auth.signInWithEmailAndPassword(eemail, pass);
    promise.catch(e => console.log(e.message));

    firebase.auth().onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
            setCookie('prijavljen', true, 30);
            setCookie('email', eemail, 30);
            window.location.href = "index.html";
        } else {
           // console.log("not logged");
        }
    });
}
